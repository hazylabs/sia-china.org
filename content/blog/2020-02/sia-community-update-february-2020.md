---
title: "Sia 社区更新 - 2020年2月"
author: "Steve"
trans: "SiaXU"
origin: "https://blog.sia.tech/sia-community-update-february-2020-658c2431ab7e"
date: 2020-02-25
type: "post"
draft: false
categories:
    - "update"
tags:
    - "update"
---

![Sia Logo](images/2020-02/sia.png)

已经过去了几个月。首先，我要感谢自上次社区更新以来您耐心等待。我很高兴地说，我们错过几个月的原因是因为事情很忙，我们所有人都忙得不可开交，除此之外，我们还有很多其他更新。

尽管如此，还是希望一切在2020年初能恢复正常。最近几个月给Sia带来了令人难以置信的进步，我认为准确地说，最新版本是我们有史以来最重要的发布版本。

v1.4.3带来了 Skynet ，这是有史以来第一次提供 Sia 文件共享和内容分发功能。我们雇用了更多的开发人员，取得了其他令人难以置信的进步，并宣传了与 Sia 的新重要集成。

我们开始吧。

## 在此更新中

- *天网*
- *SiaStream 进入测试版*
- 成长中的团队
- 团队文章
- 媒体上
- *社区焦点*
- *交易所状况*
- *开发人员更新*

## 天网

![天网](images/2020-02/skynet.png)

[天网就在这里](https://blog.sia.tech/skynet-bdf0209d6d34)。文件共享。内容分发。这是针对开发人员的去中心化CDN（内容分发网络）。它是数据和应用程序的永久场所。

> 天网的到来代表着新航团队的转折点。过去5年中的大部分时间，我们都说该技术尚未准备就绪，并且会适时进行营销。但是今天，我很高兴地宣布：**技术已经准备就绪**。
>
> *   Sia首席开发人员兼 Nebulous 联合创始人David Vorick

您可以从自己的 Sia 节点使用 Skynet（天网），也可以从 Webportal（门户）上传到 Skynet（天网）。

### 上传到天网

最重要的命令是上传命令：

`siac skynet upload [localpath] [siapath]`

这将返回一个 Skylink，您可以将其放置在任何门户中以查看文件。只要您将文件保存在自己的租用方中，该文件就会保留在天网上。

这将返回一个 Skylink，您可以将其放置在任何门户中以查看文件。只要您将文件保存在自己的租用方中，该文件就会保留在天网上。

### 固定文件

如果您将 siad 作为门户网站运行，也可以使用命令固定其他 skyfile `siac skynet pin [skylink] [siapath]`。只要至少有一个人固定文件，该文件就将永远在 Skynet（天网）上可用。

天网作为Sia v1.4.3（我们对 Sia 的最新也是最大的更新）的一部分进行了打包。此更新主要侧重于添加 Skynet（天网）功能并为将来的更新奠定基础。

您可以立即下载1.4.3，然后转到我们的[支持站点](https://tech.us11.list-manage.com/track/click?u=5df238d9e852f9801b5f2c92e&id=d564dc9672&e=2ae61e66cc)以了解有关 Skynet（天网）可以做什么。

## SiaStream 进入测试版(Beta)

SiaStream 使用Sia提供低成本的媒体存储和快速的视频流。

![SiaStream](images/2020-02/sia-stream.png)

SiaStream 已进入 Beta 版，在接下来的几个月中，我们将扩展该Beta版以向所有人开放。现在，您可以在[siastream.tech上](https://siastream.tech/)注册以了解更多[信息](https://siastream.tech/)，一旦我们进一步宣布，您将收到我们的通知邮件。

### 看一看

如果您现在有兴趣，请访问我们的 [SiaStream 支持中心](https://support.siastream.tech/)，开始阅读和了解我们出色的应用程序可以做什么。

## 我们成长中的团队

在过去的几个月里，我们增加了三名新的团队成员，他们都是欧盟 Sia 团队的一员。每个团队成员在我们的 Discord 中自我介绍。

### Marcin

> 大家好，我叫 Marcin，是 Sia 团队的最新成员！我住在华沙，将作为 Go 的核心开发人员与欧洲团队一起工作。我以前在 MaidSafe 工作过，开发 Rust 后端。我坚信隐私，OSS 和加密项目在面对政府虐待的世界中提供自由的潜力。我最喜欢的语言是 Rust（到目前为止-仍在学习 Go！），我是 Emacs 的狂热者。除此之外，我还喜欢健身，旅行，音乐，汽车和其他东西。我很高兴能加入这个项目！

### Karol

> 大家好！我叫 Karol，很高兴宣布我刚加入 Sia 团队，是一名全职开发人员。我将从 SiaStream 项目开始负责我们的 javascript 代码库。我位于波兰华沙，之前曾在 Crunch.io 担任数据分析 Web 应用程序的前端工程师。我对与 Web 相关的一切，React 及其生态系统充满热情，我喜欢自动化和简化流程并改善开发人员体验。我很高兴成为 Sia 和这个社区的一份子！

### Ivaylo

> 大家好，我是 Ivaylo，我刚加入 Sia 担任核心开发人员！我现在居住在瑞士苏黎世，并将与 Chris，PJ 和 Marcin S 一起加入欧洲团队。过去，我几乎完成了几乎所有工作，包括 Web 开发，Android 应用程序，商业软件，SaaS ，微服务。我很高兴能进入去中心化世界，并期待着帮助 Sia 接管世界！)不进行编码时，我通常会爬山，滑雪，滑浪风帆，或者只是在当地山区远足。

## 团队文章

David 对互联网的形状，它对一个人的意义以及我们为什么需要从公司监督中收回它的意义进行了精彩的[回顾](https://blog.sia.tech/the-war-for-a-free-internet-c0a7fcc00c46)。

Nebulous 的联合创始人 Luke 发表了一篇强烈推荐的[博客文章](https://lukechampine.com/retrospective.html)介绍了`us`库及其衍生软件在第一年的进展和采用情况。`us`是 Sia 协议的另一种实现，允许开发人员与网络进行非常紧密的交互。Luke 在此基础上分析了其他开发人员进行的许多软件开发以及该项目的未来方向。

Steve 写了一篇文章，展示了[Alert System （报警系统）](https://blog.sia.tech/the-sia-alerts-system-be-more-informed-about-your-cloud-storage-9d101f62fe6e)，这是 Sia 在1.4.2.0中首次亮相的惊人新功能之一。检查一下，了解 Sia 如何告诉您更多信息！

Sia 的核心开发人员 Chris 撰写了 Sia 鲜为人知的功能之一，即[streaming 4k video content seamlessly （无缝地传输4k视频内容）](https://blog.sia.tech/streaming-meets-sia-222235817956)。

Manasi 撰写了有关[Carbon和Siacoin](https://blog.sia.tech/fiat-to-siacoin-onramp-using-credit-debit-card-da1261a9f11b)集成的文章。现在，您只需单击几下即可使用您的信用卡或借记卡购买.Siacoin！此外，对于我们所有的应用程序开发人员，您可以使用 Carbon widget/api 将此功能集成到您的应用程序中。

## 媒体上

David Vorick 接受了[Epicenter播客](https://epicenter.tv/episodes/318)的采访。他讨论了 Sia 项目的起源，去中心化存储的相关性，关于工作量证明的观点以及2018年的艰苦努力。

## 社区焦点

### Cloudly

@qhead 调侃了自己即将推出的应用程序`Cloudly`：旨在创建类似于Dropbox的体验的跨平台云存储应用程序。它允许文件夹同步，在设备之间或与其他人共享文件，并包括内置的照片和视频播放器。[立即尝试](https://cloudlyapp.com/)！

### Sia Central

Nate 宣布了[Sia Central 轻钱包](https://wallet.siacentral.com/)的 Beta 版：支持常规29个单词的种子钱包，Ledger 设备和只读钱包的钱包。它的代码是开源的，种子存储在本地，永远不会离开设备。Nate 希望在正式发布之前寻求测试人员的早期反馈。

### SiaStats

Hakkane 增加了一组工具来[SiaStats](https://www.reddit.com/r/siacoin/comments/ekw4hl/new_accounting_and_tax_reporting_tools_in/)便于会计和税务报告：交易的更好的 CSV 报告，地址余额跟踪和硬币价格跟踪页面和计算器。

### Repertory
ScottG 发布了的1.2.2版本`Repertory`和的1.1.4 版本`Repertory-UI`。Repertory 是一个桌面应用程序，可将 Sia 存储作为本地驱动器安装并在不同计算机之间共享数据。此版本具有对 Raspberry Pi 4和 CentOS 8 的支持以及错误修复。

## 交易所状况
![交易所](images/2020-02/exchange.png)



## 来自…的更新

![开发角](images/2020-02/dev-corner.png)

我们的团队正在努力完成1.4.4，这是不可思议的天网的首次后续行动。这是过去几个月到1.4.3版本中的一些重要更新。

## 其他更新

大卫增加了对租用方的[保护](https://gitlab.com/NebulousLabs/Sia/merge_requests/4000)措施，以防止出租方过度滥用的价格。根据租用方的津贴自动计算出认为定价滥用的阈值，但用户也可以选择提供手动最高限额。

当直接从本地磁盘提供文件时，Chris 为租户引入了速度和内存使用方面的[优化](https://gitlab.com/NebulousLabs/Sia/merge_requests/3989/diffs)。

Matt 修复了启动 Sia 时文件修复的[错误](https://gitlab.com/NebulousLabs/Sia/merge_requests/3980)。

Marcin 更新了`watchdog`对区块链进行监视的，该事件对租用方来说很重要，现在可以跟踪过期的合同。

Luke 用 Sia 开发人员派生的定制版本替换了 Sia 使用的[数据库](https://gitlab.com/NebulousLabs/Sia/merge_requests/4027)`bbolt`。由于将来不需要原始开发人员批准更改，因此这将允许将来更快的更新和修复。

PJ 为 Sia 添加了一项新功能：[临时帐户](https://gitlab.com/NebulousLabs/Sia/merge_requests/3921)。该协议的一个瓶颈是，对于主机方和租用方之间传输的每个数据块，必须事先交换一些往返消息以支付操作费用。这导致数据传输延迟的开销。通过与主机创建临时帐户，租用方可以预付少量款项，足以支付以下后续的文件传输操作，从而可以更快，更有效地进行上传和下载。此 MR 也是 Sia 未来其他几个功能（例如文件共享）的基础。

Chris 使 Sia 的模块即使在仍在扫描区块链时也能正常工作，从而使钱包[解锁](https://gitlab.com/NebulousLabs/Sia/merge_requests/3971)`wallet`并能够立即生成新地址。

GitHub 的`shakamd`贡献了[Python bindings](https://github.com/lukechampine/us-bindings/pull/3)到`us-bindings`项目中，应用开发者可以从 Python 使用`us`！

## 总结

对于 Sia 来说，这是一个美好的几个月。我们的团队正在成长，并且我们拥有令人难以置信的动力。天网功能强大，并为网络带来了惊人的功能。

您可以期望在2020年取得伟大的成就。我们将竭尽所能。

## Steve
> *steve@sia.tech*
> *steve＃4381在Discord上*
