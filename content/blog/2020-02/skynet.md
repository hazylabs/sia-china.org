---
title: "天网（Skynet）"
author: "David Vorick"
trans: "SiaXu"
origin: "https://blog.sia.tech/skynet-bdf0209d6d34"
date: 2020-02-25
type: "post"
categories:
    - "skynet"
tags:
    - "skynet"
---

> 这是 Sia 博客关于“天网（Skynet）”的中文翻译。作者是 David Vorick ，他是 NebulousLabs 的 CEO（前者是 Sia 的母公司）。基于 Sia 去中心化存储的天网于2020年2月18日（美东时间）发布。天网提供了一个分布式 CDN 和文件共享平台作为分权互联网的基础。

![skynet](images/2020-02/skynet.jpg)


我们今天宣布的是经过20多年的工程和运营努力的产物。这段时间的绝大部分都花在了组建的困境上。几乎每周我们都会收到一个问题：“什么时候市场应用？什么时候可以建立业务开发团队？”，我们只有一直回答：“技术还没有准备就绪”。经过社区和投资者的多年耐心，我们终于可以向前迈进。技术已经准备就绪。
 天网（Skynet）是数据和应用程序的永久场所。这是一个完全公平的基础，可以在其中部署文件和应用程序，这些文件和应用程序的寿命将远远超过其原始上传程序。与比特币很像，天网是一个独立的有机体，在市场和激励机制上运作，而不是云服务器和人工干预。

可以在天网（Skynet）上部署的最简单的东西是文件。以[这个视频](https://siasky.net/CABAB_1Dt0FJsxqsu_J4TodNCbCGvtFf1Uys_3EgzOlTcg)为例（*中国连接可能会很慢，原因是缺少中国节点*），该视频由天网用户固定，因此该视频可供全世界使用。在任何时候，任何人都可以自己固定视频来进行增强。只要有一个人选择固定视频，该视频就将保持在线状态并向全世界提供。他甚至不需要自己上网。天网拥有用于托管视频并确保高正常运行时间和流畅播放的基础架构。

天网也非常适合托管应用程序。以[这个演示](https://siasky.net/CAAVU14pB9GRIqCrejD7rlS27HltGGiiCLICzmrBV0wVtA)为例，这个演示是一个允许任何人将文字消息上传到天网的应用程序。与视频类似，只要至少一个人愿意固定该应用程序，该应用程序将保持可用。同样适用于视频，固定不需要自己在线即可将应用程序提供给全世界。天网将为它提供正常运行时间。

天网的永久性来自固定机制。固定使他有权自行确定某事有权在全球范围内存在。天网是一种发布媒体，没有任何标准，要求和服务条款。

---

## 上传与下载

通过Sia节点进行上传和固定。天网实际上是建立在Sia之上的第2层。使用最新版本，任何人都可以使用 `siac skynet upload` 命令将文件部署到 skynet。输出是形式为“sia://TAACfPM8uKM-DG0rbefiQPUUBYfDew4BEaQJd7xeizMltw”的 Skylink 链接。然后，世界各地的任何人都可以使用该 Skylink 访问已上传的文件或应用程序。

通过天网门户网站进行下载。任何人都可以运行门户网站，并且每个门户网站都可以访问天网（Skynet）上的每个文件。运行门户网站的费用比运行典型的 Sia 节点高出约10美元/月，但与运行典型的Sia节点相同。

大多数人不需要自己运行门户。最初，Sia 社区的一些成员正在运行门户，这些门户可让希望下载文件的任何人免费访问。最终，访客将能够通过诸如闪电网络之类的支付工具使用微交易从门户网站执行下载。访客只需要访问一个门户即可访问所有天网（Skynet）内容。

## 网络兼容性

社区提供的天网门户实际上是称为 Webportals 的增强门户。顾名思义，可以从网络浏览器访问这些门户。它们提供了用于上传和下载的简单界面，还公开了供开发人员使用的 API。

在撰写本文时，社区提供门户的列表包括：

[https://siasky.net](https://siasky.net) — 由 Nebulous 提供

[https://skydrain.net](https://skydrain.net) — 由 PixelDrain 提供

[https://sialoop.net](https://sialoop.net) — 由 Keops 提供

[https://siacdn.com](https://siacdn.com) — 由 Maxint LLC 提供

[https://skynethub.io](https://skynethub.io) — 由 jchauan 提供

[https://skynet.luxor.tech](https://skynet.luxor.tech) — 由 Luxor 提供

[https://skynet.tutemwesi.com](https://skynet.tutemwesi.com) — 由 Tutemwesi 提供

[https://vault.lightspeedhosting.com](https://vault.lightspeedhosting.com) — 由 Lightspeed Hosting 提供

任何人都可以自由开启自己的网络门户。您可以在 [https://github.com/NebulousLabs/skynet-webportal](https://github.com/NebulousLabs/skynet-webportal) 中找到更多信息。

## 开发者支持

天网还为开发人员提供了丰富的 SDK。目前已为 Go、Python 和 Node.js 提供支持库。在未来的几个月中，我们将扩展 SDK，以支持更多的语言并具有更多的功能。

默认情况下，开发 SDK 与 siasky.net 网络门户对话。所有语言的 SDK 都支持自定义，允许开发人员选择其他网络门户，甚至指定在本地主机上运行的 Sia 节点。这些 SDK 目前支持上传和下载，并且支持固定功能。

## 加密

普通的天网（Skynet）网络上的文件是未加密的，并且可以向世界公开。用户始终可以在将文件放到天网上之前对其进行加密，以保护其隐私，但是由于天网旨在用作发布平台，因此天网默认情况下未加密。这与Sia相反，后者用于个人数据，因此是默认加密的。

目前正在开发对加密的功能支持。用户将能够创建自己的加密组并在天网（Skynet）上发布仅在该组中可用的文件。通过向他们发送加密密钥链接，可以将他们添加到该加密组。

## 分权化

天网由 Sia 区块链提供支持，Sia 区块链是一种工作量证明区块链，严格遵循比特币的设计原则。Sia没有任何链上管理，没有任何自动更新，没有定期计划的硬叉，也没有从事软分叉。在构建Sia网络时做出的每个设计决策都将分散性作为首要任务。该代码是完全开源的，可从 [https://gitlab.com/NebulousLabs/Sia](https://gitlab.com/NebulousLabs/Sia) 获得。

Sia 区块链用于驱动一种称为文件合约的特殊类型的智能合约。文件合同是上传者与主机之间的协议，要求主机在特定时间段内存储特定数据。为了赢得该合同，如果主机不遵守合同，将被没收的一定数量的SC。Sia使用概率默克尔证明(Merkle)来确定主机是否按承诺存储了数据。

当数据上传到 Sia（包括 Skynet 网络）时，数据会上传到大量不同的主机，以确保单个不良主机，甚至大量不良主机，都不足以破坏文件的稳定性。使用 Reed-Solomon 编码冗余上传文件。
建立分权自由的互联网

天网最重要的事情也许就是可用性。文件和应用程序可以在几秒钟内部署。下载的速度与传统 Web 竞争。开发人员只需编写不到12行代码即可将其应用程序与 Skynet 集成。

这些工具已完成，并且网络已经上线，直接[尝试天网](https://siasky.net/)。

