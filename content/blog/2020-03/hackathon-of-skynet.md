---
title: "天网（Sia）黑客马拉松的想法"
author: "Manasi"
trans: "SiaChina"
origin: "https://gitcoin.co/hackathon/skynet?"
date: 2020-02-29
type: "post"
draft: false
categories:
  - "skynet"
tags:
  - "hackathon"
---

![gitcoin](images/2020-03/hackathon.png)

gitcoin

- 备份工具
- 视频流应用程序（如Plex）的扩展
- 天网上托管的游戏
- JAMstack集成
- 适用于https://dappnode.io/的Skynet集成
- 创建指向文件的唯一链接的应用。让您限制可以从唯一链接下载文件的次数。这是用于销售数字商品的网上商店，以确保不只是共享文件。
- Wetransfer克隆：上传的文件会在7天后自动删除。下载文件后，您会收到一条消息/电子邮件。
- 托管：您的客户端将加密货币放入托管，该托管在您上传文件时释放。链接会自动与客户端共享。
- 文件池：让您将文件池在一起。如果共享的文件被其他3个参与者接受，则可以访问共享文件。此外，限制每个池的文件类型。例如，仅为.wav文件和.cvs文件创建池。
- 书签和共享其他Skynet应用程序和文件的工具。
- 无需使用集中门户即可浏览天网的桌面应用。
- 将现有应用程序从https://awesome.ipfs.io/迁移 到Skynet
- 将天网纳入pydio https://pydio.com/的单元格

------

### 基于天网的示例应用

- 天网上的另类Pastebin：https://siasky.net/CAAVU14pB9GRIqCrejD7rlS27HltGGiiCLICzmrBV0wVtA
- 天网视频播放列表制作器：https://siasky.net/nAAJroJW0Z7cYcpev1VVjJXXGU1WF8YQ8eUs8dJHTDEXqQ
- 天网音频播放列表制作器：https://siasky.net/IAC_-QuVMlNuwI11kMZOz7w5qeYTL8Bsh0FJfIyp_hcX0Q
- 天网博客工具：https://github.com/NebulousLabs/skynet-blogger
