---
title: "30天的新平台：50个天网应用"
author: "玛纳斯·沃拉Manasi"
trans: "SiaChina"
origin: "https://blog.sia.tech/30-days-to-50-skynet-apps-bootstrapping-a-new-platform-d1d7537ca1a6"
date: 2020-03-25
type: "post"
draft: false
categories:
  - "skynet"
tags:
  - "Winners"
---

###天网黑客马拉松项目和优胜者

上个月，我们推出了[Skynet](https://siasky.net/)，这是一个为应用程序开发人员设计的分散式文件共享和网络托管平台。天网使高速，低成本和卓越的基础架构成为自由Internet]的存储基础。

通过[适用](https://support.siasky.net/article/hrshqsn9wz-integrating-skynet)于流行编程语言的简单[API](https://sia.tech/docs/#skynet)和[SDK](https://support.siasky.net/article/hrshqsn9wz-integrating-skynet)，Skynet使开发人员能够轻松地将分散式存储集成到其应用程序中。重要的是，最终用户可以直接在Skynet上访问文件，而无需运行完整的节点或处理加密货币。

> “科学技术进步通常是逐步发生的。但是有时候，每个人都意识到一个艰巨的里程碑，在这个里程碑中，每个人都意识到了多年努力的结果。今天，我们将体验数据存储和分发的其中一个里程碑” – [Hakkane]on Skynet版本

我们发起了一系列黑客马拉松，以探索天网的巨大潜力并使开发人员了解天网所带来的可能性。黑客马拉松是创新和创造力的沃土，我们非常高兴与大家分享我们在不到一个月的时间内使用天网构建的各种应用程序！

![img](https://sia.80000.ml/max/2092/0*TYY1kgZQQa5G1AlC)

[天网AppStore](https://skynethub.io/_B19P18DEI4Y_37wg6yXtfulEq2U8AcIO_lWM7sKncC1rw/index.html#/apps/all)：探索天网应用程序和内容

# 内部黑客马拉松

天网发布后，我们非常激动，我们举办了一次内部黑客马拉松，开始在这个令人惊叹的平台上进行构建。我们的团队在两天内创建了一系列Skynet项目[-Skydo](https://github.com/m-cat/skydo)一个待办事项列表应用程序，[Skynet播放列表](https://github.com/peterjanbrone/skynet-playlist)以创建视频播放列表，[SkySync](https://github.com/MSevey/skysync)监视文件夹并将其同步到Skynet。

[这些项目](https://siasky.net/AAD0KImFYiHgH6NWwgevCmewM0_BzQdIPkpMhs5Ee7PCXA)的[列表](https://siasky.net/AAD0KImFYiHgH6NWwgevCmewM0_BzQdIPkpMhs5Ee7PCXA)可在名为[Skybin的](https://siasky.net/CAAVU14pB9GRIqCrejD7rlS27HltGGiiCLICzmrBV0wVtA)分散式[Pastebin中找到](https://siasky.net/CAAVU14pB9GRIqCrejD7rlS27HltGGiiCLICzmrBV0wVtA)（另一个黑客马拉松项目）！

![img](https://sia.80000.ml/max/1200/1*yDy_cJ6XKu71TqSNj-O3Yw.png)

Skybin —分散式Pastebin

# 麻省理工学院黑客马拉松

在[麻省理工学院的比特币世博](https://mitbitcoinexpo.org/)跑了黑客马拉松着力构建栈和扩大blockchain技术的实用性。24小时的[黑客马拉松](https://mit-btc-expo-hackathon-2020.devpost.com/)将学生和专业人员聚集在一起，进行了一个周末的编码，学习，实验和协作。

## 麻省理工学院大奖和Sia赛道冠军

## [SkyPages](https://siasky.net/CABMLShr4jkikvB15sd9cIP8LrfrmVePO24NzZE7lM5VTQ)

由一群麻省理工学院的学生创建的SkyPages赢得了总体黑客马拉松，这是所有提交的最佳项目，并获得了Sia赛道奖。Skypages使您能够以安全且分散的方式托管网站。我们很高兴看到这些学生对天网的热情，并为他们一天的建造所打动。

> [演示与示范](https://twitter.com/SiaTechHQ/status/1237108467770560515?s=20)

![img](https://sia.80000.ml/max/627/0*wcBe1RJ5pgKGznWa)

SkyPages —麻省理工学院黑客马拉松获奖者

# 虚拟黑客马拉松

我们与[Gitcoin](https://gitcoin.co/)合作举办了为期两周的[虚拟黑客](https://blog.sia.tech/skynet-virtual-hackathon-94587a3d689b)[马拉松](https://gitcoin.co/)，高质量的提交数量令我们感到惊讶。我们在整个黑客马拉松中所看到的发展和激情令人敬畏，并充分说明了天网实现的用例。老实说，有180多个注册者和40多个项目提交，我们很难选出3个亚军。

> “我在这里看到了很多精彩的项目。参加这次黑客马拉松既有趣又有趣”-Hacker

**合格项目**的**完整列表**可在[此处](https://gitcoin.co/hackathon/projects/skynet/)或Gitcoin 的[项目页面](https://gitcoin.co/hackathon/projects/skynet/)上找到。

## **大奖Prize**

## [Skylive](https://skylive.coolhd.hu/)

在DaWe建立Skylive之前，我们甚至不确定在Skynet上是否可以进行实时流式传输。DaWe克服了种种困难，开发了一个令人印象深刻的应用程序，包括为社区的很大一部分进行了[现场演示](https://siasky.net/EACSRCJLMtS-P6tpGNr1ZMCGFBbWXKoNKTHV_3l81jLE1Q)。我们对Skylive的未来可能性感到非常兴奋。祝贺DaWe赢得100万个SC。

> [*示范影片*](https://siasky.net/AAA5tBYYnuMhMl1qRV-bphSTyPsZ9JlAtKkJ21gJhSEu2g/index.html)

![img](https://sia.80000.ml/max/2666/1*HJHRP4ljWNk4N9EgeLbMLQ.png)

SkyLive —在Skynet上托管的实时HLS视频流

## 亚军

## [天网iOS应用](https://apps.apple.com/us/app/skynet-file-sharing/id1501444724?ls=1)

tetek开发的iOS应用程序使您可以直接从iPhone将媒体上传到天网。此应用程序已在Apple应用程序商店中提供，并允许日常用户轻松地与世界其他地方共享他们的照片和视频。该应用程序不需要运行Sia节点，安装后即可立即运行！

> [示范影片](https://siasky.net/_BXTC-qNUUB7x2Hy2KLAzjjWCie-5fZy-nU5cBsbD5diVw)

![img](https://sia.80000.ml/max/1330/1*uYzfBRVKCrzgPTPXM58xIQ.png)

天网上的iOS FileSharing应用

## [InstaSkynet](https://github.com/kunalkamble/instaskynet-webapp)

kunalkamble制作的InstaSkynet是一个纯天网应用程序，这意味着您可以完全从天网使用该应用程序。它是一个目录浏览器，可让您查看单个Skylink中的所有文件和文件夹。对于包含媒体内容（例如图像）的Skylink，InstaSkynet提供了漂亮的拼贴布局，使用户可以轻松查看和浏览所有内容。

> [示例文件夹](https://instasky.net/?skylink=AAB7ej1Pz319dpNJY7v_iThyVnA-hLu7IjUCvC8ufGBcdA)

![img](https://sia.80000.ml/max/2756/1*TTBe_n7ntuBe6KFOLD2c_A.png)

随着生态系统的发展，我们看到InstaSkynet和类似工具不仅用于内容共享和媒体文件夹共享，而且在制作高级应用程序时也用作关键的调试元素。例如，在[**天网上**][托管](https://siasky.net/EAByx9EagK71BoS1za93Q9s0l7xSe7VG6P8aUEtJK4mmDQ/build/index.html#/swap)的[**Uniswap**]前端具有许多组成元素，能够从天网实时探索这些元素可以帮助开发人员更好地理解和调试应用程序。

## [dgit](https://github.com/quorumcontrol/dgit)

dgit是由[Tupelo](https://www.tupelo.org/)团队构建的多项目工具。dgit允许项目在分散的后端托管git repos，将Skynet与Tupelo结合使用，以确保开发人员始终可以访问其github repos，并确保文件具有较高的正常运行时间。dgit在分散工具链和消除对集中产品的依赖方面迈出了重要一步。

> [产品网站](https://dgit.dev/)

![img](https://sia.80000.ml/max/914/1*ecm6iPFjXCRS_G0eNfWyjQ.png)

基于Tupelo和天网的dgit

## 荣誉奖

## [天网AppStore](https://skynethub.io/skapps)

天网AppStore是一个跟踪天网项目和天网媒体的网站。创建应用程序并上传要共享的内容时，可以将其发布到AppStore上，其他人可以在其中找到它。

## [天网Lisky播放器](https://github.com/redsolver/lisky_player)

Lisky-player是一个多项目应用程序，结合了Skynet和[Dat协议，](https://dat.foundation/)可以在Skynet上存储和播放音频，例如音乐和播客。

## Sky游戏

现在，您可以在Skynet上玩各种游戏，从《毁灭战士》到《井字游戏》。[末日](https://siasky.net/AACpZe2ozCdeM1KPk2Va88loscRpbgHSMLmvY1dkoKJg_Q/docs/index.html)，[2048](https://siasky.net/CABdIet8HmwG981H6RQYVxtFJwy7Lc0WFoA_6JJYcwsgcg)，[3-1游戏](https://siasky.net/_A75NGp4qw51U2auLap92VOzMoSOIgCJ4VM3jft7sfJVuw/index.html)，[SkyAir](https://siasky.net/PALsProcYDcl0R3N9QnBR7418YwP2QC2h89TtnmfLP7t3A)。

![img](https://sia.80000.ml/max/1920/1*mRabWWVjKGfQkI-xHyRs5Q.png)

在天网上玩末日审判

# 开发者提示

我们要**感谢大家**参与我们的黑客马拉松，并在Skynet上构建经过深思熟虑和有用的应用程序。Sia团队将继续为您的项目提供支持，并帮助您将其变成产品。我们建议在Discord上加入＃app-dev频道。有关在Sia上构建的项目的品牌准则，请参见[此处](https://support.sia.tech/article/yh3r6tb68h-brand-guidelines)。

此外，我们每月举办一次针对我们应用程序开发的社区电话。作为新的Sia应用程序开发人员，我们欢迎您致电这些电话。请根据自己的喜好填写此[表单](https://www.surveymonkey.com/r/XWN2BNP B)以加入他们。

------

我们刚刚接触到了天网的潜力，我们不能足够强调这种技术的革命性和巨大意义！我们建议您尝试这些[应用程序，](https://siasky.net/AADAN1bEPvwBYAqmsdck2YZvdH3-7d4vu7naH6QiyLZbQw)并亲自体验天网的魔力。

我们还有更多的公告，发布，功能更新，黑客松，计划中的活动。要保持最新状态，请在[**Discord**](https://discord.gg/sia)上加入我们，或在[**推上**]()关注我们。

[新亚博客](https://blog.sia.tech/?source=post_sidebar--------------------------post_sidebar-)

#### 与Sia和cryptocurrency相关的博客文章。

[跟随]()



> 感谢史蒂夫。 