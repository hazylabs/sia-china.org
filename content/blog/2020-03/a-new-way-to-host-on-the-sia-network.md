---
title: "在Sia网络上托管的新方法"
author: "Nate"
trans: "SiaChina"
origin: "https://blog.sia.tech/announcing-sia-host-manager-4ccf7530e874"
date: 2020-03-02
type: "post"
draft: false
categories:
  - "host"
tags:
  - "sia central"
---

![Sia主机管理器](images/2020-03/sia-host-manager.png)

*试用超棒的Sia主机管理器新界面*

Sia的命令行界面非常强大。有很多配置选项，其中大多数完全未被注意。使用命令行可以很容易地查看单个合同，哪些合同正在使用您的存储空间或您将获得多少收入。有了所有这些数据，就可以调整和优化价格。

随着Sia v1.4.0的出现，时尚，反应灵敏且令人惊叹的新钱包。始终将命令行与主机的UI分开的一件事是控制。UI不允许您访问许多主机配置选项或财务。只是显示空间不足。

由[Sia Central](https://siacentral.com/host-manager)宣布Sia主机管理器。一个专用于管理Sia网络上的主机的开源桌面应用程序。


![Sia Central](images/2020-03/sia-central.png) 

**Sia Host Manager**（Sia主机管理器）提供了快速轻松地访问与Sia主机相关的所有数据和配置的信息。它结合了来自Sia节点的数据和来自Sia Central的数据，从而可以更准确地查看主机的财务状况和运行状况。这使主机可以专注于最重要的事情。完成合同并管理价格。

您可以在[GitHub](https://github.com/siacentral/host-manager)上找到Sia Host Manager的源代码，并在[Sia Central](https://siacentral.com/host-manager)上发布它。

## 更好的组合

![](images/2020-03/sia-central-to.png) 

Sia Host Manager能够完全独立地工作，也可以与Sia-UI一起工作，以访问Sia必须提供的所有其他重要功能，例如租用存储或发送Siacoin。

Sia-UI和Sia Host Manager可以共享同一个钱包，因此您的数据将始终在两个应用程序之间同步。但是，为避免同步问题，最好使用Sia Host Manager对您的主机进行所有更改，而不要使用Sia-UI 的*Host*选项卡。

## 开始使用

就像设置Sia-UI一样，使用Sia Host Manager进行设置和入门非常容易。首先，转到[下载页面](https://siacentral.com/host-manager/download)并下载适用于您的操作系统的最新版本。支持64位Windows，macOS和Linux。

![](images/2020-03/sia-central-web.png)
下载并运行安装程序后，将为您提供简短的安装程序。如果您是第一次安装Sia，则必须创建一个新的钱包或从种子中恢复。否则，您可以自动导入现有数据。

![创建钱包](images/2020-03/sia-central-wallet.png) 

## 随便浏览

![](images/2020-03/sia-central-look.png) 

设置完成后，您将进入*Dashboard*页面。查看左侧的导航窗格，首先看到的是连接状态。如果您遇到连接问题，请单击该图标将打开Sia Central Host Troubleshooter，并执行一些常见的诊断并提供有用的故障排除提示。

您可以通过单击`bell`图标来查看任何警报。这可能包括连接问题，合同失败，可用抵押品，可用存储空间或配置问题。

通过单击`齿轮`图标，您可以访问应用程序配置选项，例如设置显示货币。您可以使用许多不同的菲亚特和加密货币（例如USD，GBP，EUR，BTC或ETH）显示财务和配置。

## 添加存储空间

现在，您已经完成了基本设置，可以开始考虑存储数据了。这可以通过单击导航窗格中的“ *存储”*菜单项来完成。

![](images/2020-03/sia-central-storage.png) 

单击“ *添加文件夹”*按钮将允许您选择新存储文件夹的位置和大小。添加文件夹后，可以通过单击存储文件夹列表中的图标之一来调整大小或删除它。

![](images/2020-03/sia-central-storage-folder.png) 

# 补足SC

要开始在Sia Network上托管，您需要填写主机钱包。估计您将需要使用多少Siacoin `total amount of storage(总存储量) * collateral per terabyte(每T押金)`。汇款到您的新的钱包，点击`wallet`图标导航窗格或*接收Siacoin*上的* Dashboard(仪表盘)*。

![](images/2020-03/sia-central-top-up.png) 

## 设定价格

![](images/2020-03/sia-central-prices.png) 

Sia网络上的每个主机都可以设置自己的价格。“ *Configuration(配置)”*页面上提供了所有主机设置以及有用的描述和平均值。这使您可以轻松地与网络比较、调整价格。

![](images/2020-03/sia-central-prices-setting.png) 

如果您更改显示货币，则会出现一个复选框，可让您固定价格。上列出它会自动调整你的定价Siacoin当前汇率[CoinGecko](https://www.coingecko.com/en/coins/siacoin)。

所有配置选项均允许您以首选单位输入值：

*   对于持续时间或时间，您可以将值设置为`14 days(14天)`或`2 weeks(2周)`。

*   使用Siacoin时，可以将值设置为`50 KSC`或`5 mSC`。

*   使用数据大小时，可以将值设置为`5 TB`或`5 TiB`。

## 监控财务

![](images/2020-03/sia-central-monitor.png) 

一旦开始与承租人订立合同，“ *Contracts(合同)”*页面将使您可以快速访问潜在收入，已赚取的收入和即将到期的合同。这使您可以更好地了解主机的财务状况。它会自动隐藏过时或未使用的合同，向您显示最重要的数据。

## 坚持下去

Sia主机需要24/7全天候运行，以使租用方可以访问其数据并完成存储合同。主机应努力至少90％的时间可以访问。

幸运的是，您可以关闭Sia Host Manager而不关闭Sia。您始终可以在系统托盘中找到它。隐藏Sia Host Manager时，所有新警报将在受支持的系统上显示为桌面通知。

![](images/2020-03/sia-host-manager-tray.png) 

如果确实需要重新启动Sia，请单击托盘中的图标，然后从菜单中单击“退出”。
