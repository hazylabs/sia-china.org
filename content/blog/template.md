---
title: "天网（Skynet）"
author: "Sia"
trans: "Sia China"
origin: "https://sia-china.org"
draft: true
date: 2020-02-25
type: "post"
categories:
    - "default"
tags:
    - "default"
---

title 标题
author 作者
trans 译者
origin 原文链接
draft 是否为草稿 true 是 false 否，请不要在正式发布前修改为 false
date 时间
type: "post" 文章 无需修改
categories 分类 最好添加到已有分类内
tags 标签

关于图片使用，例：![skynet](images/2020-02/skeynet.png) 文件放在 `static/images/2020-02/skynet.png` 内

修改`---`内的文章属性后，在 `---` 后开始写作
